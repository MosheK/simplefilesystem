#include <stdio.h>
#define Pi 3.14159
#define Cylinder_volume(r, h) (Pi * (r * r) * h)
#define Cone_volume(r, h) (Cylinder_volume(r, h) / 3)
#define Mission(r, h) (Cylinder_volume(r, h) - Cone_volume(r, h))

float main(){
    float r = 3;
    float h = 5;

    printf("%f\n", Mission(r, h));
}