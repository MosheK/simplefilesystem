#include <stdio.h>
#define Approx(y) (y * ( 1.5F - ( (number * 0.5F) * (y) * (y))))
#define Hex 0x5f3759df

float Q_rsqrt( float number )
{
	long i;
	float y;

	y  = number;
	i  = * ( long * ) &y; 
	i  = Hex - ( i >> 1 ); 
	y  = * ( float * ) &i;
	y  = Approx(y);   
	//y  = Approx(y);   
	return y;
}

int main() {
    float a = Q_rsqrt(100);
    printf("%f", a);
}